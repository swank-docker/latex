#Use an official hub.docker.com image as parent image
FROM debian:testing-slim


# Update Package Index
# Install Build Utilities
RUN apt-get update && apt-get install -y \
    make \
    latex-mk

# Install LaTeX Packages
# The following listing are package dependencies for texlive-full
#    https://packages.debian.org/buster/texlive-full
# Instead of installing the texlive-full package, only install a subset
RUN apt-get install -y \
    # -------------------------------
    # script-based vector graphics language inspired by MetaPost 
    asymptote \

    # Much-augmented BibTeX replacement for BibLaTeX users 
    # biber \

    # Finds typographic errors in LaTeX 
    chktex \

    # TeX font package (full version) with CM (EC) in Type1 in T1, T2*, TS1, X2 enc 
    cm-super \

    #    powerful TeX format 
    context \

    #    Manipulate .dvi files 
    dvidvi \

    #    convert DVI files to PNG graphics 
    dvipng \

    #    set of LaTeX macros for creating Feynman diagrams 
    # feynmf \

    #    use of psfrag constructs with pdflatex 
    fragmaster \

    #    Standalone GNU Info documentation browser 
    # info \

    #    Simple syntax checker for LaTeX 
    lacheck \

    #    installs all LaTeX CJK packages (Chinese/Japanese/Korean)
    # latex-cjk-all \

    #    utility to mark up significant differences between LaTeX files 
    latexdiff \

    #    Perl script for running LaTeX the correct number of times 
    latexmk \

    #    tools for OpenType, multiple-master, and Type 1 fonts 
    lcdf-typetools \

    #    scalable PostScript and OpenType fonts based on Computer Modern 
    lmodern \

    #    course prerequisite chart editor for LaTeX/TikZ 
    # prerex \

    #    PostScript document handling utilities 
    psutils \

    #    creates EPS files usable in TeX and pdfTeX 
    purifyeps \

    #    Collection of simple Type 1 font manipulation programs 
    t1utils \

    #    scalable PostScript and OpenType fonts based on URW Fonts 
    tex-gyre \

    #    Documentation system for on-line information and printed output 
    # texinfo \

    #    TeX Live: Essential programs and files 
    texlive-base \

    #    TeX Live: BibTeX additional styles 
    texlive-bibtex-extra \

    #    Binaries for TeX Live 
    texlive-binaries \

    #    TeX Live: TeX auxiliary programs 
    texlive-extra-utils \

    #    TeX Live: Graphics and font utilities 
    texlive-font-utils \

    #    TeX Live: Additional fonts 
    texlive-fonts-extra \

    #    TeX Live: Documentation files for texlive-fonts-extra 
    # texlive-fonts-extra-doc \

    # Links to all fonts originally in TeX Live, but not shipped with texlive-fonts-extra
    # texlive-fonts-extra-links \

    #    TeX Live: Recommended fonts 
    texlive-fonts-recommended \

    #    TeX Live: Documentation files for texlive-fonts-recommended 
    # texlive-fonts-recommended-doc \

    #    TeX Live: Additional formats 
    texlive-formats-extra \

    #    TeX Live: Games typesetting 
    texlive-games \

    #    TeX Live: Humanities packages 
    texlive-humanities \

    #    TeX Live: Documentation files for texlive-humanities 
    # texlive-humanities-doc \

    #    TeX Live: Arabic 
    # texlive-lang-arabic \

    #    TeX Live: Chinese 
    # texlive-lang-chinese \

    #    TeX Live: Chinese/Japanese/Korean (base) 
    # texlive-lang-cjk \

    #    TeX Live: Cyrillic 
    # texlive-lang-cyrillic \

    #    TeX Live: Czech/Slovak 
    # texlive-lang-czechslovak \

    #    TeX Live: US and UK English 
    texlive-lang-english \

    #    TeX Live: Other European languages 
    # texlive-lang-european \

    #    TeX Live: French 
    # texlive-lang-french \

    #    TeX Live: German 
    # texlive-lang-german \

    #    TeX Live: Greek 
    # texlive-lang-greek \

    #    TeX Live: Italian 
    # texlive-lang-italian \

    #    TeX Live: Japanese 
    # texlive-lang-japanese \

    #    TeX Live: Korean 
    # texlive-lang-korean \

    #    TeX Live: Other languages 
    # texlive-lang-other \

    #    TeX Live: Polish 
    # texlive-lang-polish \

    #    TeX Live: Portuguese 
    # texlive-lang-portuguese \

    #    TeX Live: Spanish 
    # texlive-lang-spanish \

    #    TeX Live: LaTeX fundamental packages 
    texlive-latex-base \

    #    TeX Live: Documentation files for texlive-latex-base 
    # texlive-latex-base-doc \

    #    TeX Live: LaTeX additional packages 
    texlive-latex-extra \

    #    TeX Live: Documentation files for texlive-latex-extra 
    # texlive-latex-extra-doc \

    #    TeX Live: LaTeX recommended packages 
    texlive-latex-recommended \

    #    TeX Live: Documentation files for texlive-latex-recommended 
    # texlive-latex-recommended-doc \

    #    TeX Live: LuaTeX packages 
    texlive-luatex \

    #    TeX Live: MetaPost and Metafont packages 
    texlive-metapost \

    #    TeX Live: Documentation files for texlive-metapost 
    # texlive-metapost-doc \

    #    TeX Live: Music packages 
    texlive-music \

    #    TeX Live: Graphics, pictures, diagrams 
    texlive-pictures \

    #    TeX Live: Documentation files for texlive-pictures 
    # texlive-pictures-doc \

    #    TeX Live: Plain TeX packages 
    texlive-plain-generic \

    #    TeX Live: PSTricks 
    texlive-pstricks \

    #    TeX Live: Documentation files for texlive-pstricks 
    # texlive-pstricks-doc \

    #    TeX Live: Publisher styles, theses, etc. 
    texlive-publishers \

    #    TeX Live: Documentation files for texlive-publishers 
    # texlive-publishers-doc \

    #    TeX Live: Mathematics and science packages 
    texlive-science \

    #    TeX Live: Documentation files for texlive-science 
    # texlive-science-doc \

    #    TeX Live: XeTeX and packages 
    # texlive-xetex \

    #    system for processing phonetic symbols in LaTeX 
    # tipa \

    #    Qt interface to prerex, a course prerequisite chart editor 
    # vprerex \

# Cleanup
# NOTE: official docker debian images call apt-get clean -y automatically
# NOTE: this line continues from previous RUN
#       allows to comment/uncomment without looking for last line break \
# && apt-get clean -y
&& rm -rf /var/lib/apt/lists/*
