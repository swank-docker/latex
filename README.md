# Docker LaTeX / TeX Live

This docker file can be used to create a docker image with a LaTeX / TeX Live
installation. The original purpose of the docker image was for automated
build of LaTeX documents through continous integration (CI).

The listed packages are extracted from the Debian latex-full meta package.
Packages can be enabled or disabled by commenting out the package names.